function isObjEmpty(obj) {
    return Object.keys(obj).length === 0;
}

async function convert(req, res) {
    if (req.method !== "GET") {
        return res.status(405).json({
            status: {
                code: 405,
                message: "Method not supported",
            },
        });
    }

    if (isObjEmpty(req.query)) {
        return res.status(400).json({
            status: {
                code: 400,
                message: "Missing information",
            },
        });
    }

    if (typeof req.query.from === "undefined") {
        return res.status(400).json({
            status: {
                code: 400,
                message: "Missing parameter 'from'",
            },
        });
    }

    if (typeof req.query.to === "undefined") {
        return res.status(400).json({
            status: {
                code: 400,
                message: "Missing parameter 'to'",
            },
        });
    }

    if (typeof req.query.amount === "undefined") {
        return res.status(400).json({
            status: {
                code: 400,
                message: "Missing parameter 'amount'",
            },
        });
    }

    async function fetchApi(url, options) {
        try {
            const response = await fetch(url, options);

            if (!response.ok) {
                return { error: await response.text() };
            }

            const json = await response.json();

            return json;
        } catch (error) {
            return { error: error };
        }
    }

    const apiHeaders = new Headers();
    apiHeaders.append("apikey", process.env.API_ACCESS_KEY);

    const apiOptions = {
        method: "GET",
        redirect: "follow",
        headers: apiHeaders,
    };

    const apiUrl = `https://api.apilayer.com/exchangerates_data/convert?to=${req.query.to}&from=${req.query.from}&amount=${req.query.amount}`;

    const convertedExchange = await fetchApi(apiUrl, apiOptions);

    if (typeof convertedExchange.error !== "undefined") {
        return res.status(400).json({
            status: {
                code: 400,
                message: `Exchange API problem: ${convertedExchange.error}`,
            },
        });
    }

    return res.status(200).json({
        status: {
            code: 200,
        },
        convertedExchange: convertedExchange,
    });
}

export default convert;
