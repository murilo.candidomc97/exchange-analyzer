import Form from "../components/Form";

function Home() {
    return (
        <div>
            <h1>Converter</h1>
            <Form />
        </div>
    );
}

export default Home;
