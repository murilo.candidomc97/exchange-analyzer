import { useState } from "react";

function Form() {
    const [dolar, setDolar] = useState();
    const [reais, setReais] = useState();

    const convert = async (e) => {
        e.preventDefault();

        async function fetchApi(url, options) {
            try {
                const response = await fetch(url, options);

                if (!response.ok) {
                    return { error: await response.text() };
                }

                const json = await response.json();

                if (json.status.code !== 200) {
                    return { error: await response.text() };
                }

                return json;
            } catch (error) {
                return { error: error };
            }
        }

        const apiHeaders = new Headers();
        apiHeaders.append("Content-Type", "application/json; charset=UTF-8");

        const apiOptions = {
            method: "GET",
            headers: apiHeaders,
        };

        const apiUrl = `https://exchange-analyzer.vercel.app/api/convert?from=USD&to=BRL&amount=${dolar}`;

        const apiResponse = await fetchApi(apiUrl, apiOptions);

        if (typeof apiResponse.error !== "undefined") {
            return console.error(apiResponse.error);
        }

        setReais(Math.round(apiResponse.convertedExchange.result));
    };

    return (
        <form>
            <label>Dólar</label>
            <br />
            <input onChange={(e) => setDolar(e.target.value)} type="text" />
            <br />
            <label>Reais:{reais}</label>
            <strong></strong>
            <br />
            <button type="submit" onClick={convert}>
                Converter
            </button>
        </form>
    );
}

export default Form;
